package com.eoh.service;


import com.eoh.Constants;
import com.eoh.PropertyLoader;
import com.eoh.entity.Summary;
import com.eoh.entity.SystemIntegrationAudit;
import com.eoh.repository.SystemIntegrationAuditDAO;
import com.eoh.utils.HealthCheckUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.text.DateFormat;
import java.util.*;

@Service
public class SystemIntegrationAuditImpl implements SystemIntegrationAuditService {



    private SystemIntegrationAuditDAO systemIntegrationAuditDAO;

    @Autowired
    public SystemIntegrationAuditImpl(SystemIntegrationAuditDAO systemIntegrationAuditDAO) {
        this.systemIntegrationAuditDAO = systemIntegrationAuditDAO;
    }

    public  List<Summary> radioActiveSummary() {
        Date toDate = Calendar.getInstance().getTime();
        Date fromDate = getFromDate(toDate);

        List<SystemIntegrationAudit> radioActiveSystemIntegrationAudits = this.getSystemIntegrationAuditsForRange();
        List<Summary> radioActiveSummary = HealthCheckUtils.getSummaryList(radioActiveSystemIntegrationAudits);

        return radioActiveSummary;
    }

    public List<SystemIntegrationAudit> getSystemIntegrationAuditsForRange() {
        Date toDate = Calendar.getInstance().getTime();
        Date fromDate = getFromDate(toDate);

        String[] expectedExceptions = PropertyLoader.getInstance().getProperties().getProperty("exception.expected").split(",");
        List<SystemIntegrationAudit> systemIntegrationAudits = this.systemIntegrationAuditDAO.getSystemIntegrationAuditsForRange(Constants.LONG_DATE.format(fromDate), Constants.LONG_DATE.format(toDate));

        systemIntegrationAudits.forEach(systemIntegrationAudit -> {
            if (systemIntegrationAudit.getStack_trace() == null) {
                systemIntegrationAudit.setStack_trace("");
            }

            if (systemIntegrationAudit.getCall_ended() != null) {
                long timeTakenMs = systemIntegrationAudit.getCall_ended().getTime() - systemIntegrationAudit.getCall_started().getTime();
                DateFormat df = Constants.SHORT_DATE;
                Date d3 = new Date(timeTakenMs);
                df.setTimeZone(TimeZone.getTimeZone("UTC"));
                systemIntegrationAudit.setTimeTaken(df.format(d3));
            } else {
                systemIntegrationAudit.setTimeTaken(null);
            }



            if (systemIntegrationAudit.getStatus() == 1) {
                systemIntegrationAudit.setStatusValue(Constants.KEY_SUCCESS);
            } else if (systemIntegrationAudit.getStatus() == 2) {
                if (systemIntegrationAudit.getStack_trace() != null) {
                    if (this.isExpectedException(expectedExceptions, systemIntegrationAudit.getStack_trace())) {
                        systemIntegrationAudit.setStatusValue("exception (expected)");
                    } else {
                        systemIntegrationAudit.setStatusValue(Constants.KEY_EXCEPTION);
                    }
                }
            } else {
                systemIntegrationAudit.setStatusValue(Constants.KEY_NO_RESP);
            }
        });

        return systemIntegrationAudits;
    }


    @Override
    public SystemIntegrationAudit findSystemIntegrationaAditById(int id) {
        return systemIntegrationAuditDAO.findSystemIntegrationaAditById(id);
    }

    private boolean isExpectedException(String[] expectedExceptions, String stackTrace) {
        if (expectedExceptions != null && expectedExceptions.length > 0) {
            for (String exception : expectedExceptions) {
                if (stackTrace.contains(exception)) {
                    return true;
                }
            }
        }

        return false;
    }

    private static Date getFromDate(Date toDate) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(toDate);
        cal.add(Calendar.MINUTE, -1);
        return cal.getTime();
    }

}
