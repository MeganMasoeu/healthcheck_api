package com.eoh.service;

import com.eoh.entity.SystemIntegrationAudit;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Qualifier("healthcheck")
public interface SystemIntegrationAuditService {
    List<SystemIntegrationAudit> getSystemIntegrationAuditsForRange();
    SystemIntegrationAudit findSystemIntegrationaAditById(int id);
}
