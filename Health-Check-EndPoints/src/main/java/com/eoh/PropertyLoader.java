package com.eoh;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Properties;

public class PropertyLoader {
    private static PropertyLoader instance = null;
    private Properties prop = new Properties();
    private String fileName = null;

    private PropertyLoader() {
        InputStream input = null;

        try {
            input = getClass().getClassLoader().getResourceAsStream("application.properties");
            this.prop.load(input);
            this.setFileName();
        } catch (IOException var11) {
            System.out.println(var11.getMessage());
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException var10) {
                    System.out.println(var10.getMessage());
                }
            }

        }

    }

    public static PropertyLoader getInstance() {
        if (instance == null) {
            instance = new PropertyLoader();
        }

        return instance;
    }

    private void setFileName() {
        if (this.fileName == null) {
            Calendar cal = Calendar.getInstance();
            this.fileName = Constants.LONG_DATE.format(cal.getTime()).replace("-", "").replace(":", "").replace(" ", "_").concat(".xlsx");
        }

    }

    public String getFileName() {
        return this.fileName;
    }

    public Properties getProperties() {
        return this.prop;
    }
}
