package com.eoh.controller;

import com.eoh.entity.Summary;
import com.eoh.entity.SystemIntegrationAudit;
import com.eoh.service.SystemIntegrationAuditImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/integration")
@CrossOrigin
public class SystemIntegrationAuditController extends BaseController{

    private SystemIntegrationAuditImpl systemIntegrationAuditImpl;

    @Autowired
    public SystemIntegrationAuditController(SystemIntegrationAuditImpl systemIntegrationAuditImpl) {
        this.systemIntegrationAuditImpl = systemIntegrationAuditImpl;
    }

    @GetMapping("/summary")
    public List<Summary> getAll(){
        return systemIntegrationAuditImpl.radioActiveSummary();
    }

    @GetMapping("/get-system-integration-audit/{audit_id}")
    public SystemIntegrationAudit findSystemIntegrationaAditById(@PathVariable("audit_id") int id){
        return systemIntegrationAuditImpl.findSystemIntegrationaAditById(id);
    }

}
