package com.eoh;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Constants {

        public static final SimpleDateFormat LONG_DATE = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        public static final DateFormat SHORT_DATE = new SimpleDateFormat("HH:mm:ss");
        public static final String KEY_EXCEPTION = "exception";
        public static final String KEY_SUCCESS = "successful";
        public static final String KEY_NO_RESP = "no response";
        public static final String KEY_PASSED = "passed";
        public static final String KEY_FAILED = "failed";
        public static final String RADIO_ACTIVE_TEST = "RadioActiveTest";
        public static final String RADIO_ACTIVE = "RadioActive";
        public static final String PERSISTENCE_MTN = "mtn";
        public static final String MAIL_SUBJECT = "Integration Health Check";
        public static final String HTML_INFO_ATT = "<p><b>Attention Support Team</b></p>";
        public static final String HTML_INFO_SLOW = "<p>There are currently slow running process(es) and/or high failure rate(s) which requires your attention. See the list(s) below:</p>";
        public static final String HTML_INFO_OK = "<p>The current status is <b style=\"color:green;\">normal</b> and there are no slow running processes at the moment.</p>";
        public static final String HTML_ERROR = "<p>The scheduled check was <b style=\"color:red;\">unable</b> to run due to the following exception:</p>";
        public static final String HTML_HDR_RADIO_ACTIVE = "<p><b>Radio Active</b></p>";
        public static final String HTML_HDR_HIGH_LATENCY = "<p>High Latency<p>";
        public static final String HTML_HDR_HIGH_FAILURE = "<p>High Failure Rate<p>";
        public static final String HTML_FOOTER_ATTACHMENT = "<p>Health Check Attached</p>";
        public static final String HTML_FOOTER_REGARDS = "<p>Regards</p>";
        public static final String HTML_FOOTER_APP_NAME = "<p><b>Integration Health Check Application</b></p>";
        public static final String DIV_START = "<div style=\"font-family:calibri;\">";
        public static final String DIV_END = "</div>";
        public static final String LI_STYLE_RED = "<li style=\"color:red;\">";
        public static final String LI_STYLE_ORANGE = "<li style=\"color:orange;\">";
        public static final String UL_START = "<ul>";
        public static final String UL_END = "</ul>";
        public static final String PROD = "prod";

}
