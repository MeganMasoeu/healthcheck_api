package com.eoh.utils;

import com.eoh.Constants;
import com.eoh.entity.Summary;
import com.eoh.entity.SummaryEntity;
import com.eoh.entity.SystemIntegrationAudit;

import java.util.*;

public class HealthCheckUtils {

    public static List<SummaryEntity> convertToSummaryEntityList(List<Summary> lstSummary, String application, long runId) {
        List<SummaryEntity> ret = new ArrayList<>();
        Date d = new Date();

        for (Summary summary : lstSummary) {
            SummaryEntity summaryEntity = new SummaryEntity();
            summaryEntity.setApplication(application);
            summaryEntity.setRunDate(d);
            summaryEntity.setRun_id(runId);
            summaryEntity.setComponent(summary.getComponent());
            summaryEntity.setFunction(summary.getFunction());
            summaryEntity.setStatus(summary.getStatus());
            summaryEntity.setStatusCount(summary.getCount());
            summaryEntity.setAverageTime(summary.getTimeTakenInMs());
            ret.add(summaryEntity);
        }

        return ret;
    }

    public static List<Summary> getSummaryList(List<SystemIntegrationAudit> radioActiveSystemIntegrationAudits) {
        Map<String, Summary> summaryMap = new HashMap<>();

        for (SystemIntegrationAudit systemIntegrationAudit : radioActiveSystemIntegrationAudits) {
            String status = systemIntegrationAudit.getStatus() == 1 ? Constants.KEY_PASSED : Constants.KEY_FAILED;
            String functionName = systemIntegrationAudit.getMethod_name() + "_" + status;
            Summary summary;
            if (summaryMap.containsKey(functionName)) {
                if (systemIntegrationAudit.getCall_ended() != null) {
                    summary = summaryMap.get(functionName);
                    summary.setCount(summary.getCount() + 1);
                    long avgtimeTakenMs = (systemIntegrationAudit.getCall_ended().getTime() - systemIntegrationAudit.getCall_started().getTime() + summary.getTimeTakenInMs()) / 2L;
                    Constants.SHORT_DATE.setTimeZone(TimeZone.getTimeZone("UTC"));
                    Date d3 = new Date(avgtimeTakenMs);
                    summary.setTimeTakenInMs(avgtimeTakenMs);
                    summary.setTimeTakenFormatted(Constants.SHORT_DATE.format(d3));
                    summary.setStatus(systemIntegrationAudit.getStatusValue());
                }
            } else if (systemIntegrationAudit.getCall_ended() != null) {
                summary = new Summary();
                summary.setComponent(systemIntegrationAudit.getComponent_name());
                summary.setCount(1);
                summary.setFunction(systemIntegrationAudit.getMethod_name());
                summary.setTimeTakenFormatted(systemIntegrationAudit.getTimeTaken());
                summary.setTimeTakenInMs(systemIntegrationAudit.getCall_ended().getTime() - systemIntegrationAudit.getCall_started().getTime());
                summaryMap.put(functionName, summary);
                String s = systemIntegrationAudit.getStatusValue();
                summary.setStatus(systemIntegrationAudit.getStatusValue());
            }
        }

        List<Summary> lstSummary = new ArrayList<>();

        for (Map.Entry<String, Summary> entry : summaryMap.entrySet()) {
            lstSummary.add(entry.getValue());
        }

        lstSummary.sort(Comparator.comparing(Summary::getComponent));
        return lstSummary;
    }
}
