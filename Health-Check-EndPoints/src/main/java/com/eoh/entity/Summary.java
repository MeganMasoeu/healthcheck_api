package com.eoh.entity;

import lombok.Data;

@Data
public class Summary {
    private String component;
    private String function;
    private int count;
    private String status;
    private String timeTakenFormatted;
    private long timeTakenInMs;

    public String toString() {
        return "Summary{component=" + this.component + ", function=" + this.function + ", count=" + this.count + ", status=" + this.status + ", timeTakenFormatted=" + this.timeTakenFormatted + ", timeTakenInMs=" + this.timeTakenInMs + '}';
    }
}