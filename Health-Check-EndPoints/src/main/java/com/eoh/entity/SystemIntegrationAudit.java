package com.eoh.entity;


import com.eoh.Constants;
import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Data
@NamedQueries({@NamedQuery(
        name = Constants.RADIO_ACTIVE,
        query = "SELECT s " +
                "FROM SystemIntegrationAudit s " +
                "WHERE call_started between to_date(:fromDate, 'yyyy-mm-dd hh24:mi:ss') " +
                "AND to_date(:toDate, 'yyyy-mm-dd hh24:mi:ss')"
)})
public class SystemIntegrationAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long audit_id;

    private Short status;

    @Transient
    private String statusValue;

    private Date call_started;

    private Date call_ended;

    @Transient
    private String timeTaken;

    private String component_name,method_name,parameters,stack_trace;

    public SystemIntegrationAudit() {
    }

    public SystemIntegrationAudit(Long audit_id) {
        this.audit_id = audit_id;
    }

    public SystemIntegrationAudit(Long audit_id, String component_name, String method_name) {
        this.audit_id = audit_id;
        this.component_name = component_name;
        this.method_name = method_name;
    }
}


