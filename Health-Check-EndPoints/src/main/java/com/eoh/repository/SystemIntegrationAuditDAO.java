package com.eoh.repository;

import com.eoh.Constants;
import com.eoh.entity.SystemIntegrationAudit;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Qualifier("systemIntegration")
public class SystemIntegrationAuditDAO {

    @PersistenceContext
    private EntityManager em;

    public List<SystemIntegrationAudit> getSystemIntegrationAuditsForRange(String fromDate, String toDate) {

        try {

            Query systemIntegrationAuditsForRange = em.createNamedQuery(Constants.RADIO_ACTIVE);
            systemIntegrationAuditsForRange.setParameter("fromDate", fromDate);
            systemIntegrationAuditsForRange.setParameter("toDate", toDate);
            return systemIntegrationAuditsForRange.getResultList();
        } finally {
            em.close();
        }

    }

    public SystemIntegrationAudit findSystemIntegrationaAditById(int audit_id){
        SystemIntegrationAudit systemIntegrationAudit = null;

        try{
            systemIntegrationAudit = em.find(SystemIntegrationAudit.class, audit_id);
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            em.close();
        }

        return systemIntegrationAudit;
    }

}
